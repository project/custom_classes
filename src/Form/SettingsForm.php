<?php

namespace Drupal\custom_classes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_classes\Controller\CustomClassController;
use Psr\Container\ContainerInterface;

/**
 * Configure custom_classes settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The custom class controller service.
   *
   * @var \Drupal\custom_classes\Controller\CustomClassController
   */
  protected $customClassController;

  /**
   * Constructs a new service instance.
   *
   * @param \Drupal\custom_classes\Controller\CustomClassController $customClassController
   *   The custom class controller service.
   */
  public function __construct(CustomClassController $customClassController) {
    $this->customClassController = $customClassController;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('custom_classes.controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_classes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['custom_classes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('custom_classes.settings')->get('class_mappings');

    $form['class_mappings'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Name'),
      '#description' => $this->t(
        'Pattern: <br>routename;url;form_id;form_element_path_with_php;class1-to-add class2-new;class3-to-remove class4<br>Examples:<br>@pattern1<br>@pattern2<br>@pattern3', [
          '@pattern1' => 'entity.commerce_product.canonical;*;*;$form["actions"]["submit"];cta;',
          '@pattern2' => '*;*;node_article_form;$form["actions"]["submit"];;button--primary',
          '@pattern3' => '*;/node/*/edit;*;$form["actions"]["preview"];preview-button;',
        ],
      ),
      '#rows' => 20,
      '#default_value' => empty($settings) ? '' : $settings,
    ];

    $form['preview'] = [
      '#type' => 'details',
      '#title' => $this->t('Preview of the saved settings in a table'),
    ];
    $tableHeader = [
      $this->t('Route name'),
      $this->t('URL'),
      $this->t('Form ID'),
      $this->t('Path to element with PHP syntax'),
      $this->t('Classes to add'),
      $this->t('Classes to remove'),
    ];
    $classMappingsArray = $this->customClassController->stringToCsv($settings);

    $form['preview']['table'] = [
      '#type' => 'table',
      '#header' => $tableHeader,
      '#rows' => $classMappingsArray,
      '#empty' => $this->t('Nothing is set.'),
      '#title' => $this->t('Preview of the saved settings in a table'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $classMappings = $form_state->getValue('class_mappings');

    if (!empty($classMappings)) {
      $classMappingsArray = $this->customClassController->stringToCsv($classMappings);

      foreach ($classMappingsArray as $i => $item) {
        if (count($item) < 5) {
          $form_state->setErrorByName(
            'class_mappings',
            $this->t('Incorrect entry on line @rowNr. A row must consist of the following: <br>routename;url;form_id;form_element_path_with_php;class1-to-add class2-new;class3-to-remove class4', [
              '@rowNr' => $i + 1,
            ])
          );
          return;
        }

        if (empty(trim($item[CustomClassController::INDEX_CLASSES])) &&
          empty(trim($item[CustomClassController::INDEX_CLASS_TO_REMOVE]))
        ) {
          $form_state->setErrorByName(
            'class_mappings',
            $this->t('ncorrect entry on line @rowNr. You must add a new class or specify the removal of an existing one.', [
              '@rowNr' => $i + 1,
            ])
          );
          return;
        }

      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('custom_classes.settings')
      ->set('class_mappings', $form_state->getValue('class_mappings'))
      ->save();
  }

}
