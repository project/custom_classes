<?php

namespace Drupal\custom_classes\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\custom_classes\FormElementRemoveClasses;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure custom_classes settings for this site.
 */
class CustomClassController extends ControllerBase {

  /**
   * An alias manager to find the alias for the current system path.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  const INDEX_ROUTE = 0;
  const INDEX_PATH = 1;
  const INDEX_FORM_ID = 2;
  const INDEX_PATH_TO_KEY = 3;
  const INDEX_CLASSES = 4;
  const INDEX_CLASS_TO_REMOVE = 5;

  /**
   * CustomClassController constructor.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   Alias manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   Path matcher.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   Current path.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The rouute match.
   */
  public function __construct(
    AliasManagerInterface $aliasManager,
    PathMatcherInterface $pathMatcher,
    RequestStack $requestStack,
    CurrentPathStack $currentPath,
    ConfigFactoryInterface $config_factory,
    RouteMatchInterface $route_match
  ) {
    $this->aliasManager = $aliasManager;
    $this->pathMatcher = $pathMatcher;
    $this->requestStack = $requestStack;
    $this->currentPath = $currentPath;
    $this->configFactory = $config_factory;
    $this->routeMatch = $route_match;
  }

  /**
   * Convert string lines to CSV.
   */
  public function stringToCsv($csvData) {
    $lines = explode(PHP_EOL, $csvData);
    $array = [];
    foreach ($lines as $line) {
      $array[] = str_getcsv($line, ';', '\'');
    }
    return $array;
  }

  /**
   * Returns saved config values.
   */
  protected function getConfigValues() {
    $settings = $this->configFactory->get('custom_classes.settings');
    $classMappingsString = $settings->get('class_mappings');
    $classMappings = $this->stringToCsv($classMappingsString);
    $classMappingsResult = [];
    foreach ($classMappings as $delta => $classMapping) {
      if (is_array($classMapping) && count($classMapping) >= 5) {
        // Route.
        $configFormRoute = trim($classMapping[self::INDEX_ROUTE]);
        if ($configFormRoute === '') {
          $configFormRoute = '*';
        }
        $classMappingsResult[$delta][self::INDEX_ROUTE] = $configFormRoute;

        // Path.
        $configFormPath = trim($classMapping[self::INDEX_PATH]);
        if ($configFormPath === '') {
          $configFormPath = '*';
        }
        $classMappingsResult[$delta][self::INDEX_PATH] = $configFormPath;

        // Form ID.
        $configFormId = trim($classMapping[self::INDEX_FORM_ID]);
        if ($configFormId === '') {
          $configFormId = '*';
        }
        if (strpos($configFormId, '*') !== FALSE) {
          $configFormId = str_replace('*', '.*', $configFormId);
        }
        $classMappingsResult[$delta][self::INDEX_FORM_ID] = "/^{$configFormId}$/Um";

        // Path to key.
        $configFormPathToKey = trim($classMapping[self::INDEX_PATH_TO_KEY]);
        $configFormPathToKey = str_replace('$form', '|', $configFormPathToKey);
        $configFormPathToKey = str_replace('"', '|', $configFormPathToKey);
        $configFormPathToKey = str_replace(']', '|', $configFormPathToKey);
        $configFormPathToKey = str_replace('[', '|', $configFormPathToKey);
        $classMappingsResult[$delta][self::INDEX_PATH_TO_KEY] =
          preg_split('@\|@', $configFormPathToKey, -1, PREG_SPLIT_NO_EMPTY);

        // Classes.
        $configFormClasses = trim($classMapping[self::INDEX_CLASSES]);
        $classesArray = preg_split('@ @', Xss::filter($configFormClasses), -1, PREG_SPLIT_NO_EMPTY);
        if (!empty($classesArray)) {
          foreach ($classesArray as $i => $item) {
            $classesArray[$i] = Html::getClass($item);
          }
        }
        $classMappingsResult[$delta][self::INDEX_CLASSES] = $classesArray;

        // Classes to remove.
        $configFormClassToRemove = trim($classMapping[self::INDEX_CLASS_TO_REMOVE]);
        $classToRemoveArray = preg_split('@ @', $configFormClassToRemove, -1, PREG_SPLIT_NO_EMPTY);
        $classMappingsResult[$delta][self::INDEX_CLASS_TO_REMOVE] = $classToRemoveArray;
      }
    }

    return $classMappingsResult;
  }

  /**
   * Alters form where class add and removal logic is attached.
   */
  public function customClassesFormAlter(&$form, FormStateInterface &$form_state, $form_id) {
    $classMappings = $this->getConfigValues();

    // If there are no override rules, we quit.
    if (!$classMappings) {
      return;
    }

    foreach ($classMappings as $classMapping) {
      $configFormRoute = $classMapping[self::INDEX_ROUTE];
      $configFormPath = $classMapping[self::INDEX_PATH];
      $configFormId = $classMapping[self::INDEX_FORM_ID];
      $configFormPathToKey = $classMapping[self::INDEX_PATH_TO_KEY];
      $configFormClasses = $classMapping[self::INDEX_CLASSES];
      $configFormClassesToRemove = $classMapping[self::INDEX_CLASS_TO_REMOVE];

      if ($configFormRoute !== '' && $configFormRoute !== '*' && !$this->checkRoute($configFormRoute)) {
        continue;
      }

      if ($configFormPath !== '' && $configFormPath !== '*' && !$this->evaluate($configFormPath)) {
        continue;
      }

      if ($configFormId === '*' || preg_match($configFormId, $form_id)) {
        // Classes to add.
        $element = (NestedArray::keyExists($form, $configFormPathToKey)) ?
          NestedArray::getValue($form, $configFormPathToKey) :
          [];

        if (empty($element["#attributes"]["class"])) {
          $element["#attributes"]["class"] = [];
        }
        $element["#attributes"]["class"] =
          array_merge($element["#attributes"]["class"], $configFormClasses);

        // Classes to remove.
        if (!empty($configFormClassesToRemove)) {
          // Here the FormElement process and pre_render did not run yet.
          // So, class list is incomplete.
          // We will remove the classes when this list is final.
          // That is, in the last method call before the FormElement renderer,
          // in FormElementRemoveClasses::doRemoveClasses method.
          // The following doesn't work:
          // $element["#pre_render"][] =
          // [FormElementRemoveClasses::class, 'doRemoveClasses'];
          // If we do this, we override the Button pre_render methods,
          // and only the FormElementRemoveClasses::doRemoveClasses would run.
          // We therefore use a workaround:
          // - if we add it to the process methods, it does not overwrite
          //   the previous ones
          // - In this method we add the pre_render method. Because then
          //   we are no longer overwriting what we have already done.
          //
          // To this pre_render method we pass under the #class_to_remove
          // render array key what to remove. It's because the pre_render
          // is only affected to that 1 form element, not the whole form.
          $element["#class_to_remove"] = $configFormClassesToRemove;

          $element["#process"][] = [$this, 'processButton'];
        }

        NestedArray::setValue($form, $configFormPathToKey, $element, TRUE);
      }

    }

  }

  /**
   * Custom pre render function is added to button form element.
   */
  public static function processButton(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here we append the method for removing classes to the pre_render methods.
    // Unfortunately, we can't do this in form alter right away,
    // because it would break the other pre_render methods.
    // This is suspected to be due to a bug.
    $element["#pre_render"][] = [
      FormElementRemoveClasses::class,
      'doRemoveClasses',
    ];
    return $element;
  }

  /**
   * Path matching evaluation.
   *
   * Returns true when path is matched with the given pages rule.
   */
  protected function evaluate($pages) {
    if ($pages === '*' || $pages === '' || $pages === '.*') {
      return TRUE;
    }
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = mb_strtolower($pages);
    if (!$pages) {
      return TRUE;
    }

    $request = $this->requestStack->getCurrentRequest();
    // Compare the lowercase path alias (if any) and internal path.
    $path = $this->currentPath->getPath($request);
    // Do not trim a trailing slash if that is the complete path.
    $path = $path === '/' ? $path : rtrim($path, '/');
    return $this->pathMatcher->matchPath($path, $pages);

    // Path aliases would result in performance degradation.
    // These should be considered for use.
    // $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));
    // return $this->pathMatcher->matchPath($path_alias, $pages) ||
    // (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
    // Needs evaluation if we include this in the module.
  }

  /**
   * Route matching evaluation.
   *
   * Returns true when route is matched with the given route.
   */
  protected function checkRoute($addedroute = "*") {
    $route = $this->routeMatch;
    $routeName = $route->getRouteName();
    return ($routeName === $addedroute);
  }

}
