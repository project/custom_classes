<?php

namespace Drupal\custom_classes;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Custom pre render logic.
 *
 * The reason:
 *
 * Render #pre_render callbacks must be methods of a class that implements
 * \Drupal\Core\Security\TrustedCallbackInterface or be an anonymous function.
 *
 * Therefore we implement our own pre_render logic in a separate class.
 *
 * The final class list will only be available after the process and
 * pre_render functions of the form elements derived from FormElement,
 * so this code should be run after those
 *
 * The post_process is no good either,
 * because there you would have to manipulate the Markup.
 */
class FormElementRemoveClasses implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['doRemoveClasses'];
  }

  /**
   * Custom callback where we remove classes from form elements.
   */
  public static function doRemoveClasses($element) {
    // Only do something when you have specified
    // which classes should be removed.
    if (!empty($element['#class_to_remove'])) {
      $classesToRemove = $element['#class_to_remove'];

      // If there is no class, there is nothing more to do.
      if (empty($element["#attributes"]["class"])) {
        return $element;
      }

      foreach ($classesToRemove as $classToRemove) {
        if (($key = array_search($classToRemove, $element["#attributes"]["class"])) !== FALSE) {
          unset($element["#attributes"]["class"][$key]);
        }
      }

      unset($element['#class_to_remove']);
    }

    return $element;
  }

}
