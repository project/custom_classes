# Custom Classes

## Description

Adds or removes classes from form elements based on the URL or route 
name or Form ID.For example if a module doesn't define a unique 
class for a button,you can add one with this module. You don't 
have to create a form alter hook for it.

Also, be careful when removing classes as this can result 
in AJAX errors on forms where a certain button is missing 
a required class. (For example if you remove the success 
class from the commerce Add to cart button, it could result
to LogicException: The database connection is not serializable errors)

## Set up module
1. After installing the module go to /admin/config/system/custom-classes page.
2. Fill the values in CSV format separated by ; (there's help text).
3. You can see the saved settings in a table below the textarea form.
4. If your forms are cached, you need to rebuild
   caches after the configuration change.
